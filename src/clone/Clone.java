
/*
* Programmed by Muhammad Ahsan
* muhammad.ahsan@gmail.com
* Erasmus Mundus Master in Data Mining and Knowledge Management
* France - Italy
 */
package clone;

/**
 *
 * @author Ahsan
 */
class Book implements Cloneable {
    String Name;
    String Author;

    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}


public class Clone {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws CloneNotSupportedException {
        Book B = new Book();

        B.Name   = "Java";
        B.Author = "Muhammad Ahsan";

        Book C = (Book) B.clone();

        C.Name = "C#";
        System.out.println(B.Name + " " + B.hashCode());
        System.out.println(C.Name + " " + C.hashCode());
    }
}
